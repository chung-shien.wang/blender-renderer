"""
Run this script using:

    blender -b --python same_pose_renderer_all.py 
    
"""

import argparse
import numpy as np
import os
import sys
sys.path.append('/Volumes/Data/GitLab/Blender-renderer')

import util
import blender_interface

img_dir = '/Volumes/Data/GitLab/Blender-renderer/SRN_Dataset_part'
output_dir = '/Volumes/Data/GitLab/Blender-renderer/SRN_new_render'
obj_dir = '/Volumes/Data/GitLab/Blender-renderer/SRN_object_part'

def get_subdirectories(base_dir, directory=True):
    subdirectories = [os.path.join(base_dir, d) for d in os.listdir(base_dir) if (not directory) or os.path.isdir(os.path.join(base_dir, d))]
    return subdirectories

# 1. save pose 
# 2. save original image 
# 3. save new rendered image 

def save_pose(choosed_poses):
    for i in choosed_poses:
        saved_location = os.path.join(*i.split('/')[-3:-1])
        saved_location = os.path.join(output_dir, saved_location)
        if not os.path.exists(saved_location):
            os.makedirs(saved_location)
        os.system(f"cp {i} {saved_location}")
        
def save_img(choosed_poses):
    for i in choosed_poses:
        base_dir = os.path.join(*i.split('/')[:-2], 'rgb')
        item_num = i.split('/')[-1].split('.')[0]
        img = os.path.join(base_dir, item_num + ".png")
        saved_location = os.path.join(*img.split('/')[-3:-1])
        saved_location = os.path.join(output_dir, saved_location)
        if not os.path.exists(saved_location):
            os.makedirs(saved_location)
        os.system(f"cp /{img} {saved_location}")
        
def get_obj_path(obj):
    obj_name = obj.split('/')[-1]
    return os.path.join(obj_dir, obj_name, "models/model_normalized.obj")

def save_obj(pose_path, obj_path):
    save_path = os.path.join(output_dir, pose_path.split('/')[-3], "new_rendered")
    temp_save_path = os.path.join(save_path, "buffer")
    if not os.path.exists(save_path):
        os.makedirs(save_path)
        
    original_png_path = os.path.join(temp_save_path, "rgb/000000.png")
    png_name = (pose_path.split('/')[-1]).split('.')[0]
    new_png_path = os.path.join(save_path, f"{png_name}.png")

    # ============================================================
    # render pipeline
    renderer = blender_interface.BlenderInterface(resolution=128)
    obj_location = np.zeros((1,3))
    # read camera pose from txt file 
    cam_pose = util.read_pose(pose_path)
    blender_poses = [util.cv_cam2world_to_bcam2world(cam_pose)]
    obj_pose = np.eye(4)
    renderer.import_mesh(obj_path, scale=1., object_world_matrix=obj_pose)
    renderer.render(temp_save_path, blender_poses, write_cam_params=True)
    # ============================================================
    
    os.system(f"mv {original_png_path} {new_png_path}")
    os.system(f"rm -rf {temp_save_path}")
    
    
os.system(f"rm -rf {output_dir}")
os.mkdir(output_dir)     

pose_num = 5
obj_num = 10    
objects = np.random.choice(get_subdirectories(img_dir), obj_num, replace=False).tolist()

for obj in objects: 
    pose_path = os.path.join(obj, 'pose')
    poses = get_subdirectories(pose_path, directory=False)
    choosed_poses = np.random.choice(poses, pose_num, replace=False).tolist()
    
    save_img(choosed_poses)
    save_pose(choosed_poses)
    
    obj_path = get_obj_path(obj)
    for pose_path in choosed_poses:
        save_obj(pose_path, obj_path)