"""
Run this script using:

    blender -b --python parameter_estimation.py 
    
"""

import bpy 
import numpy as np 
import os 
import sys
sys.path.append('/Volumes/Data/GitLab/Blender-renderer')

import util 

param_file = '/Users/brianwang/Downloads/params.npz' # file for saved light params

class BlenderInterface():
    def __init__(self, resolution=128):
        self.resolution = resolution

        # Delete the default cube (default selected)
        bpy.ops.object.delete()
        
        # Set the world background to white 
        world = bpy.context.scene.world
        world.use_nodes = True
        bg_node = world.node_tree.nodes['Background']
        bg_node.inputs[0].default_value = (0, 0, 0, 1) 
        bpy.context.scene.view_settings.view_transform = 'Raw'

        # Deselect all. All new object added to the scene will automatically selected.
        self.blender_renderer = bpy.context.scene.render
        self.blender_renderer.resolution_x = resolution
        self.blender_renderer.resolution_y = resolution
        self.blender_renderer.resolution_percentage = 100
        self.blender_renderer.image_settings.file_format = 'PNG'  # set output format to .png
        self.blender_renderer.film_transparent = True
        self.blender_renderer.image_settings.color_mode = "RGB"
        
        # set up lighting 
        has_light = any(obj.type == 'LIGHT' for obj in bpy.context.scene.objects)
        if not has_light:
            self.lamp1 = bpy.data.lights.new(name="Light", type='SUN')
            light_object = bpy.data.objects.new("Light", self.lamp1)
            bpy.context.collection.objects.link(light_object)
            
        for obj in bpy.data.objects:
            if obj.type == 'LIGHT':
                if obj.name != 'Light':
                    obj.select_set(True)
                    bpy.context.view_layer.objects.active = obj
                else:
                    obj.select_set(False)
        bpy.ops.object.delete()
        self.lamp1 = bpy.data.lights['Light']
        self.lamp1.type = 'SUN'
        self.lamp1.use_shadow = False
        self.lamp1.specular_factor = 0.0
        
        params = {}
        with np.load(param_file) as data:
            params['energy'] = data['energy']
            params['rotx'] = data['rotx']
            params['roty'] = data['roty']
            params['angle'] = data['angle']
            params['energy2'] = data['energy2']
            params['rotx2'] = data['rotx2']
            params['roty2'] = data['roty2']
            params['angle2'] = data['angle2']
            
        self.lamp1 = bpy.data.objects['Light']
        self.lamp1.data.energy = params['energy']
        self.lamp1.rotation_euler = (params['rotx'], params['roty'], 0)
        self.lamp1.data.angle = params['angle'] 
        
        bpy.ops.object.light_add(type='SUN')
        self.lamp2 = bpy.data.lights['Sun']
        self.lamp2.use_shadow = False
        self.lamp2.specular_factor = 0.0
        self.lamp2 = bpy.data.objects['Sun']
        self.lamp2.data.energy = params['energy2']
        self.lamp2.rotation_euler = (params['rotx2'], params['roty2'], 0)
        self.lamp2.data.angle = params['angle2']
        
        """
        some checkpoint
        energy: 4.615 | rot_x: -0.356 | rot_y: 0.379 | angle: 0.067
        energy: 2.477 | rot_x: -0.209 | rot_y: 0.300 | angle: 0.045
        """
        
        # Set up the camera
        self.camera = bpy.context.scene.camera
        self.camera.data.sensor_height = self.camera.data.sensor_width # Square sensor
        util.set_camera_focal_length_in_world_units(self.camera.data, 525./512*resolution) # Set focal length to a common value (kinect)
        
        bpy.context.scene.render.engine = 'BLENDER_EEVEE'
        bpy.context.scene.eevee.use_gtao = True
        bpy.context.scene.eevee.taa_render_samples = 64

        # set up for output depth information 
        bpy.context.view_layer.use_pass_z = True
        bpy.context.scene.use_nodes = True
        tree = bpy.context.scene.node_tree
        for node in tree.nodes:
            tree.nodes.remove(node)
        render_layers = tree.nodes.new('CompositorNodeRLayers')
        composite = tree.nodes.new('CompositorNodeComposite')
        normalize = tree.nodes.new('CompositorNodeNormalize')
        alphaover = tree.nodes.new('CompositorNodeAlphaOver')

        bpy.data.scenes["Scene"].node_tree.nodes["Composite"].use_alpha = False
        self.depth_output = tree.nodes.new('CompositorNodeOutputFile')
        self.depth_output.format.file_format = "PNG"
        self.depth_output.format.color_mode = 'BW'

        tree.links.new(render_layers.outputs['Image'], alphaover.inputs[2])
        tree.links.new(alphaover.outputs['Image'] ,composite.inputs['Image'])
        tree.links.new(render_layers.outputs['Depth'], normalize.inputs[0])
        tree.links.new(normalize.outputs[0], self.depth_output.inputs[0])
          
        bpy.data.objects['Camera'].visible_shadow = True
        bpy.ops.object.select_all(action='DESELECT')

    def import_mesh(self, fpath, scale=1., object_world_matrix=None):
        ext = os.path.splitext(fpath)[-1] 
        bpy.ops.wm.obj_import(filepath=str(fpath))

        obj = bpy.context.selected_objects[0]
        #bpy.context.object.display.show_shadows = False

        """
        In the original repo, this object_world_matrix thing was used. 
        However, this should be omitted in order to render the same 
        image as used in SMR datasets given their poses.
        (This also affect how we set up the light sources.)
        """
        # if object_world_matrix is not None:
        #     obj.matrix_world = object_world_matrix
            

        bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY', center='BOUNDS')
        obj.location = (0., 0., 0.) # center the bounding box!
            
        # fix the material problem (maybe)
        for mat in bpy.data.materials:
            mat.use_backface_culling = True

        if scale != 1.:
            bpy.ops.transform.resize(value=(scale, scale, scale))

        # Disable transparency & specularities
        M = bpy.data.materials
        for i in range(len(M)):
            M[i].blend_method = 'OPAQUE'
            M[i].specular_intensity = 0.0
            
    def render(self, output_dir, blender_cam2world_matrices, write_cam_params=False):
        
        if write_cam_params: 
            img_dir = os.path.join(output_dir, 'rgb')
            pose_dir = os.path.join(output_dir, 'pose')
            depth_dir = os.path.join(output_dir, 'depth')
            
            util.cond_mkdir(img_dir)
            util.cond_mkdir(pose_dir)
            util.cond_mkdir(depth_dir)
        else:
            img_dir = output_dir
            util.cond_mkdir(img_dir)
            
        # save intrinsic params 
        if write_cam_params:
            K = util.get_calibration_matrix_K_from_blender(self.camera.data)
            with open(os.path.join(output_dir, 'intrinsics.txt'),'w') as intrinsics_file:
                intrinsics_file.write('%f %f %f 0.\n'%(K[0][0], K[0][2], K[1][2]))
                intrinsics_file.write('0. 0. 0.\n')
                intrinsics_file.write('1.\n')
                intrinsics_file.write('%d %d\n'%(self.resolution, self.resolution))

        for i in range(len(blender_cam2world_matrices)):
            self.camera.matrix_world = blender_cam2world_matrices[i]

            # Render the object
            if os.path.exists(os.path.join(img_dir, '%06d.png' % i)):
                continue
            
            # set z-depth output directory 
            self.depth_output.base_path = depth_dir
            
            # Render the color image
            self.blender_renderer.filepath = os.path.join(img_dir, '%06d.png'%i)
            bpy.ops.render.render(write_still=True, use_viewport=True)

            if write_cam_params:
                # Write out camera pose
                RT = util.get_world2cam_from_blender_cam(self.camera)
                cam2world = RT.inverted()
                with open(os.path.join(pose_dir, '%06d.txt'%i),'w') as pose_file:
                    matrix_flat = []
                    for j in range(4):
                        for k in range(4):
                            matrix_flat.append(cam2world[j][k])
                    pose_file.write(' '.join(map(str, matrix_flat)) + '\n')
                    
            # rename the depth map since it's given a stupid default name by Blender
            
            stupid_default_name = os.path.join(depth_dir, 'Image0001.png')
            new_name = os.path.join(depth_dir, f'{i:06d}.png')
            os.system(f"mv {stupid_default_name} {new_name}")

        # Remember which meshes were just imported
        meshes_to_remove = []
        for ob in bpy.context.selected_objects:
            meshes_to_remove.append(ob.data)

        bpy.ops.object.delete()

        # Remove the meshes from memory too
        for mesh in meshes_to_remove:
            bpy.data.meshes.remove(mesh)
    
    def change_energy(self, x):
        self.lamp1.data.energy = x 
        
    def change_rotation_x(self, x):
        self.lamp1.rotation_euler[0] = x
        
    def change_rotation_y(self, x):
        self.lamp1.rotation_euler[1] = x
        
    def change_angle(self, x):
        self.lamp1.data.angle = x
        
    def get_energy(self):
        return self.lamp1.data.energy
    
    def get_rotation_x(self):
        return self.lamp1.rotation_euler[0]
    
    def get_rotation_y(self):
        return self.lamp1.rotation_euler[1]
    
    def get_angle(self):
        return self.lamp1.data.angle 
    
    # --------------------------------------
    
    def change_energy2(self, x):
        self.lamp2.data.energy = x 
        
    def change_rotation_x2(self, x):
        self.lamp2.rotation_euler[0] = x
        
    def change_rotation_y2(self, x):
        self.lamp2.rotation_euler[1] = x
        
    def change_angle2(self, x):
        self.lamp2.data.angle = x
        
    def get_energy2(self):
        return self.lamp2.data.energy
    
    def get_rotation_x2(self):
        return self.lamp2.rotation_euler[0]
    
    def get_rotation_y2(self):
        return self.lamp2.rotation_euler[1]
    
    def get_angle2(self):
        return self.lamp2.data.angle 
    
          
def mse(img1, img2):
    img1 = np.array(img1.pixels[:]) * 255
    img2 = np.array(img2.pixels[:]) * 255
    return np.mean((img1.flatten() - img2.flatten())**2)


def find_grad(base_error, renderer, get_func, change_func, h, img1_dir, img2_dir, blender_poses):
    os.system(f'rm -rf {output_dir}')
    os.system(f'mkdir {output_dir}')
    x0 = get_func() # original argument
    change_func(x0 + h)
    renderer.import_mesh(obj_dir, scale=1., object_world_matrix=obj_pose)
    renderer.render(output_dir, blender_poses, write_cam_params=True)
    img1 = bpy.data.images.load(filepath = img1_dir)
    img2 = bpy.data.images.load(filepath = img2_dir)
    new_error1 = mse(img1, img2)
    
    os.system(f'rm -rf {output_dir}')
    os.system(f'mkdir {output_dir}')
    change_func(x0 - h)
    renderer.import_mesh(obj_dir, scale=1., object_world_matrix=obj_pose)
    renderer.render(output_dir, blender_poses, write_cam_params=True)
    img1 = bpy.data.images.load(filepath = img1_dir)
    img2 = bpy.data.images.load(filepath = img2_dir)
    new_error0 = mse(img1, img2)
    
    change_func(x0)
    first_deriv = (new_error1 - new_error0) / (2 * h)
    
    return first_deriv


# first order gradient descent
def update(get_func, change_func, grad, eta=1000):
    x0 = get_func()
    x = x0 - eta * grad 
    change_func(x)
    
# SGD + Momentum
def momentum(get_func, change_func, grad, v, lr, mu): 
    # v is the speed 
    x0 = get_func()
    v = mu * v - lr * grad
    v = np.clip(v, -0.01, 0.01) 
    x = x0 + v
    change_func(x)
    return x, v

# momentum for energy, different clipping 
def momentum2(get_func, change_func, grad, v, lr, mu): 
    # v is the speed 
    x0 = get_func()
    v = mu * v - lr * grad
    v = np.clip(v, -0.5, 0.5) 
    x = x0 + v
    change_func(x)
    return x, v
    
with open("/Users/brianwang/Downloads/output.txt", 'w') as f:
    pass

obj_dir = '/Users/brianwang/Downloads/ShapeNetCore.v2/02958343/f8b92a0ea7f4705c9fec71e8f4aac226/models/model_normalized.obj'
pose_base_dir = '/Users/brianwang/Downloads/f8b92a0ea7f4705c9fec71e8f4aac226/pose/'
intrinsic_dir = ''
output_dir = '/Users/brianwang/Downloads/buffer2'

os.system(f'rm -rf {output_dir}')
os.system(f'mkdir {output_dir}')

obj_location = np.zeros((1,3))
obj_pose = np.eye(4)

img1_dir = "/Users/brianwang/Downloads/buffer2/rgb/000000.png"
img2_base_dir = "/Users/brianwang/Downloads/f8b92a0ea7f4705c9fec71e8f4aac226/rgb"

# =======================
epochs = 1
iterations = 30 # per instance per epoch
# =======================

lr = 3e-4 
mu = 0.8 # momentum

for epoch in range(epochs):
    for instance_num in range(0, 250, 50):
        # initialization 
        renderer = BlenderInterface(resolution=128)
        img2_dir = [os.path.join(img2_base_dir, str(x).zfill(6)) + ".png" for x in range(0, 250)][instance_num]
        pose_dir = [os.path.join(pose_base_dir, str(x).zfill(6)) + ".txt" for x in range(0, 250)][instance_num]
        
        # momentum initialization
        v_energy = 0
        v_rotx = 0
        v_roty = 0
        v_angle = 0
        v_energy2 = 0
        v_rotx2 = 0
        v_roty2 = 0
        v_angle2 = 0
        for iter_count in range(iterations):
            grad_energy = 0
            grad_rotx = 0
            grad_roty = 0
            grad_angle = 0
            grad_energy2 = 0
            grad_rotx2 = 0
            grad_roty2 = 0
            grad_angle2 = 0
            
            # read camera pose from txt file 
            cam_pose = util.read_pose(pose_dir)
            blender_poses = [util.cv_cam2world_to_bcam2world(cam_pose)]
            renderer.import_mesh(obj_dir, scale=1., object_world_matrix=obj_pose)
            renderer.render(output_dir, blender_poses, write_cam_params=True) # save to img1_dir
            
            img1 = bpy.data.images.load(filepath = img1_dir)
            img2 = bpy.data.images.load(filepath = img2_dir)
            base_error = mse(img1, img2) 
            # grad
            grad_energy = find_grad(base_error, renderer, renderer.get_energy, renderer.change_energy, 1, img1_dir, img2_dir, blender_poses)
            grad_rotx = find_grad(base_error, renderer, renderer.get_rotation_x, renderer.change_rotation_x, 1, img1_dir, img2_dir, blender_poses)
            grad_roty = find_grad(base_error, renderer, renderer.get_rotation_y, renderer.change_rotation_y,1, img1_dir, img2_dir, blender_poses)
            grad_angle = find_grad(base_error, renderer, renderer.get_angle, renderer.change_angle, 1, img1_dir, img2_dir, blender_poses)
            grad_energy2 = find_grad(base_error, renderer, renderer.get_energy2, renderer.change_energy2, 1, img1_dir, img2_dir, blender_poses)
            grad_rotx2 = find_grad(base_error, renderer, renderer.get_rotation_x2, renderer.change_rotation_x2, 1, img1_dir, img2_dir, blender_poses)
            grad_roty2 = find_grad(base_error, renderer, renderer.get_rotation_y2, renderer.change_rotation_y2, 1, img1_dir, img2_dir, blender_poses)
            grad_angle2 = find_grad(base_error, renderer, renderer.get_angle2, renderer.change_angle2, 1, img1_dir, img2_dir, blender_poses)
            
            # update
            energy, v_energy = momentum2(renderer.get_energy, renderer.change_energy, grad_energy, v_energy, lr, mu)
            rotx, v_rotx = momentum(renderer.get_rotation_x, renderer.change_rotation_x, grad_rotx, v_rotx, lr, mu)
            roty, v_roty = momentum(renderer.get_rotation_y, renderer.change_rotation_y, grad_roty, v_roty, lr, mu)
            angle, v_angle = momentum(renderer.get_angle, renderer.change_angle, grad_angle, v_angle, lr, mu)
            energy2, v_energy2 = momentum2(renderer.get_energy2, renderer.change_energy2, grad_energy2, v_energy2, lr, mu)
            rotx2, v_rotx2 = momentum(renderer.get_rotation_x2, renderer.change_rotation_x2, grad_rotx2, v_rotx2, lr, mu)
            roty2, v_roty2 = momentum(renderer.get_rotation_y2, renderer.change_rotation_y2, grad_roty2, v_roty2, lr, mu)
            angle2, v_angle2 = momentum(renderer.get_angle2, renderer.change_angle2, grad_angle2, v_angle2, lr, mu)
            
                
            with open("/Users/brianwang/Downloads/output.txt", 'a') as f:
                f.write(f"Epoch: {epoch}\n")
                f.write(f"Image: {instance_num}\n")
                f.write(f"Iteration: {iter_count} error: {base_error}\n")
                f.write(f"energy: {renderer.get_energy():.3f} | rot_x: {renderer.get_rotation_x()*180/np.pi:.3f} | rot_y: {renderer.get_rotation_y()*180/np.pi:.3f} | angle: {renderer.get_angle()*180/np.pi:.3f}" + '\n')
                f.write(f"energy: {renderer.get_energy2():.3f} | rot_x: {renderer.get_rotation_x2()*180/np.pi:.3f} | rot_y: {renderer.get_rotation_y2()*180/np.pi:.3f} | angle: {renderer.get_angle2()*180/np.pi:.3f}" + '\n')
                f.write("----------------------\n")
        
        # save params 
        np.savez(param_file, energy=energy, rotx=rotx, roty=roty, angle=angle, energy2=energy2, rotx2=rotx2, roty2=roty2, angle2=angle2)
            

for obj in bpy.data.objects:
    if obj.type == 'LIGHT':
        if obj.name != 'Light':
            obj.select_set(True)
            bpy.context.view_layer.objects.active = obj
        else:
            obj.select_set(False)
bpy.ops.object.delete()