#!/bin/zsh

# yellow car
#obj_dir="/Users/brianwang/Downloads/ShapeNetCore.v2/02958343/10555502fa7b3027283ffcfc40c29975/models/model_normalized.obj"
# red car
#obj_dir="/Users/brianwang/Downloads/ShapeNetCore.v2/02958343/100715345ee54d7ae38b52b4ee9d36a3/models/model_normalized.obj"
# truck 
#obj_dir="/Users/brianwang/Downloads/ShapeNetCore.v2/02958343/100c3076c74ee1874eb766e5a46fceab/models/model_normalized.obj"

# test car
obj_dir=/Users/brianwang/Downloads/ShapeNetCore.v2/02958343/f8b92a0ea7f4705c9fec71e8f4aac226/models/model_normalized.obj

#output_dir="./render_output"
output_dir="/Users/brianwang/Downloads/buffer"
num_observations=10
sphere_radius=1
resolution=1024
mode="train"

rm -rf $output_dir
mkdir $output_dir
blender --background --python shapenet_spherical_renderer.py -- --output_dir "$output_dir" --mesh_fpath "$obj_dir" --num_observations "$num_observations" --sphere_radius "$sphere_radius" --resolution=$resolution --mode="$mode"
#blender --python shapenet_spherical_renderer.py -- --output_dir "$output_dir" --mesh_fpath "$obj_dir" --num_observations "$num_observations" --sphere_radius "$sphere_radius" --mode="$mode"

