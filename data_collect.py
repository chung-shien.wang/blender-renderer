#!/Users/brianwang/anaconda3/bin/python
import os 
import argparse 


search_path = '/Users/brianwang/Downloads/SRN_Dataset/cars_val'
def get_subdirectories(directory_path):
    base_dir = '/globalwork/data/ShapeNetCore.v2/02958343'
    subdirectories = [os.path.join(base_dir, d) for d in os.listdir(directory_path) if os.path.isdir(os.path.join(directory_path, d))]
    return subdirectories

obj_paths = get_subdirectories(search_path)[:30] # just collect the first 30 object


save_dir = "/Users/brianwang/Downloads/buffer"
os.system(f'rm -rf {save_dir}')
os.system(f'mkdir {save_dir}')
for i, path in enumerate(obj_paths):
    print('='*20, i, '='*20)
    os.system(f"scp -r wang_c@recog.vision.rwth-aachen.de:{path} {save_dir}")
    

