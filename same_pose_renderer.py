"""
Run this script using:

    blender -b --python same_pose_renderer.py 
    
"""

import argparse
import numpy as np
import os
import sys
sys.path.append('/Volumes/Data/GitLab/Blender-renderer')

import util
import blender_interface

obj_dir = '/Users/brianwang/Downloads/ShapeNetCore.v2/02958343/f8b92a0ea7f4705c9fec71e8f4aac226/models/model_normalized.obj'
pose_dir = '/Users/brianwang/Downloads/f8b92a0ea7f4705c9fec71e8f4aac226/pose/000000.txt'
intrinsic_dir = ''
output_dir = '/Users/brianwang/Downloads/buffer2'

os.system(f'rm -rf {output_dir}')
os.system(f'mkdir {output_dir}')


renderer = blender_interface.BlenderInterface(resolution=128)

obj_location = np.zeros((1,3))

# read camera pose from txt file 
cam_pose = util.read_pose(pose_dir)

blender_poses = [util.cv_cam2world_to_bcam2world(cam_pose)]

obj_pose = np.eye(4)

renderer.import_mesh(obj_dir, scale=1., object_world_matrix=obj_pose)
renderer.render(output_dir, blender_poses, write_cam_params=True)

