#!/bin/zsh

input_dir="/Users/brianwang/Downloads/ShapeNetCore.v2"
output_dir="/Users/brianwang/Downloads/buffer"
num_observations=10
sphere_radius=1
resolution=1080
mode="train"

rm -rf $output_dir
mkdir $output_dir

find "$input_dir" -name "*.obj" -print0 | xargs -0 -n1 -P1 -I {} blender -b --python shapenet_spherical_renderer.py -- --output_dir "$output_dir" --mesh_fpath "{}" --num_observations "$num_observations" --sphere_radius "$sphere_radius" --resolution="$resolution" --mode="$mode"
